const data = [
  {
    product_name: "Nail",
    count: 67,
    price_for_all: 364
  },
  {
    product_name: "Hammer",
    count: 43,
    price_for_all: 453
  },
  {
    product_name: "Tile",
    count: 23,
    price_for_all: 553
  }
];

const count = data.reduce((sum, current) => sum + current.count, 0); 
const price = data.reduce((sum, current) => sum + current.price_for_all, 0);

const result = {
    "count": count,
    "price": price
}

console.log(result)